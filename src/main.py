"""Sample file for running  """

import time
def check(x,y):

	"""
	Checks for positive integers with a delay to simulate a performance bottleneck.
	"""

	if x>0 and y>0:

		# If both inputs are positive, we simulate heavy computation with a sleep method
		time.sleep(x)

		return True


	else:
		return False


def add(x,y):

	"""
	Adds two positive integers.
	"""

	ok = check(x,y)
	if ok:
		return x+y
	return ok



