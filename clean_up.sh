#/bin/bash

rm VyPRServer/verdicts.db

sqlite3 VyPRServer/verdicts.db < VyPRServer/verdict-schema.sql

rm -rf instrumentation_maps/*
rm -rf binding_spaces/*
rm -rf index_hash/*
