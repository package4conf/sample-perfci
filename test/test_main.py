from cunit.parameterized_test import ParameterizedTest
from src.main import add

"""This is a test class for checking two positive integers"""

class TestMain(ParameterizedTest):


	def test_main(self):

		external_input = self.param

		x= external_input

		y= 1

		sum = add(x,y)

		self.assertGreater(sum, 0)


